import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Area;


public class BaseVectorShape {
	
	/** instance variable */
	private Shape shape;
	private Polygon polygon;
    private boolean alive;
    private double x,y;
    private double velX, velY;
    private double moveAngle, faceAngle;
	
    
    /** Default contstructor */
    BaseVectorShape() {
        setShape(null);
        setAlive(false);
        setX(0.0);
        setY(0.0);
        setVelX(0.0);
        setVelY(0.0);
        setMoveAngle(0.0);
        setFaceAngle(0.0);
    }
    
  //accessor methods
    public Shape getShape() 
    { 
    	return shape; 
    }
    public Polygon getPolygon() 
    { 
    	return polygon; 
    }
    public boolean isAlive() 
    { 
    	return alive; 
    }
    public double getX() 
    { 
    	return x; 
    }
    public double getY() 
    { 
    	return y; 
    }
    public double getVelX() 
    { 
    	return velX; 
    }
    public double getVelY() 
    { 
    	return velY; 
    }
    public double getMoveAngle() 
    { 
    	return moveAngle; 
    }
    public double getFaceAngle() 
    { 
    	return faceAngle; 
    }
    public void setShape(Shape shape) 
    { 
    	this.shape = shape; 
    }
    public void setPolygon(Polygon polygon) 
    { 
    	this.polygon = polygon; 
    }
    public void setAlive(boolean alive) 
    { 
    	this.alive = alive; 
    }
    public void setX(double x) 
    { 
    	this.x = x; 
    }
    public void incX(double i) 
    { 
    	this.x += i; 
    }
    public void setY(double y) 
    { 
    	this.y = y; 
    }
    public void incY(double i) 
    { 
    	this.y += i; 
    }
    public void setVelX(double velX) 
    { 
    		this.velX = velX; 
    }
    public void incVelX(double i) 
    { 
    	this.velX += i;	
    }
    public void setVelY(double velY) 
    { 
    		
    	this.velY = velY; 
    }
    public void incVelY(double i) 
    { 
    	this.velY += i;
    }
    public void setFaceAngle(double angle) 
    { 
    	this.faceAngle = angle; 
    }
    public void incFaceAngle(double i) 
    { 
    	this.faceAngle += i; 
    }
    public void setMoveAngle(double angle) 
    { 
    	this.moveAngle = angle; 
    }
    public void incMoveAngle(double i) 
    { 
    	this.moveAngle += i; 
    }
	public double calcAngleMoveX(double ang) 
	{
		return (double) (Math.cos(ang * Math.PI / 180));
	}
	public double calcAngleMoveY(double ang)
	{
		return (double) (Math.sin(ang * Math.PI / 180));
	}
	
	public static boolean intersects(Polygon p1, Polygon p2) 
	{ 
		if (p1 == null || p2 == null)
			return false;
		Point p; 
		for(int i = 0; i < p2.npoints;i++) 
		{ 
			p = new Point(p2.xpoints[i],p2.ypoints[i]); 
			if(p1.contains(p)) 
				return true; 
		} 
		for(int i = 0; i < p1.npoints;i++) 
		{ 
			p = new Point(p1.xpoints[i],p1.ypoints[i]); 
			if(p2.contains(p)) 
				return true; 
		} 
		return false; 
	}
}

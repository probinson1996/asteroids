import java.applet.Applet;
import javax.sound.sampled.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Asteroids extends Applet implements Runnable, KeyListener 
{

	private static final long serialVersionUID = 1L;
	
	Thread gameloop;
	BufferedImage backbuffer;
	Graphics2D g2d;
	AffineTransform identity = new AffineTransform();
	boolean showBounds = false;

	int TPS = 0;
	int targetTPS = 50;
	int ticks = targetTPS;
	long tickTimer;
	
	static int screenWidth = 640;
	static int screenHeight = 480;
	
	Random rand = new Random();
	
	InformationPanel informationPanelTop = new InformationPanel("Top");
	InformationPanel informationPanelTopTwo = new InformationPanel("TopTwo");
	
	int ASTEROIDS = 10;
	Asteroid[] ast = new Asteroid[ASTEROIDS];
	
	int STARS = 300;
	Star[] star = new Star[STARS];
	
	Ship player = new Ship(3, 6, 1, new Color(82,255,255,255));
	int ALIENS = 7;
	Ship alien[] = new Ship[ALIENS];

	static AudioInputStream sound;
	static Clip audioPlayer;
	
	Boolean leftKey = false;
	Boolean rightKey = false;
	Boolean upKey = false;
	Boolean downKey = false;
	Boolean spaceKey = false;
	
	public int score = 0;
	long gameStart = System.currentTimeMillis();
	
	public void init() 
	{
		this.setSize(screenWidth, screenHeight);
	
		backbuffer = new BufferedImage(640, 480, BufferedImage.TYPE_INT_RGB);
		g2d = backbuffer.createGraphics();
		
		player.setX(320);
		player.setY(240);
		player.setLives(3);
		player.setBounds(updateBounds(player.getPolygon(), player.getFaceAngle(), new Point.Double(player.getX(),player.getY())));
		if (player.getPolygon() == null)
			System.out.println("a");
		
		for (int n = 0; n < STARS; n++)
		{
			star[n] = new Star();
			star[n].setX((double) rand.nextInt(660) + 0);
			star[n].setY((double) rand.nextInt(500) + 0);
		}

		for (int n = 0; n < ASTEROIDS; n++) 
		{
			ast[n] = new Asteroid();
			ast[n].setRotVel(rand.nextInt(3) + 1);
			ast[n].setX((double) rand.nextInt(600) + 20);
			ast[n].setY((double) rand.nextInt(440) + 20);
			ast[n].setMoveAngle(rand.nextInt(360));
			double ang = ast[n].getMoveAngle() - 90;
			ast[n].setVelX(ast[n].calcAngleMoveX(ang));
			ast[n].setVelY(ast[n].calcAngleMoveY(ang));
			ast[n].setBounds(updateBounds(ast[n].getPolygon(), ast[n].getMoveAngle(), new Point.Double(ast[n].getX(),ast[n].getY())));
		}
		
		for (int n = 0; n < ALIENS; n++) 
		{
			alien[n] = new Ship(0,6,0.6, Color.ORANGE);
			alien[n].setX((double) rand.nextInt(600) + 20);
			alien[n].setY((double) rand.nextInt(440) + 20);
			alien[n].setFaceAngle((int)(Math.random() * 360));
			alien[n].setMoveAngle(alien[n].getFaceAngle());
			double ang = alien[n].getMoveAngle() - 90;
			alien[n].setVelX(alien[n].calcAngleMoveX(ang));
			alien[n].setVelY(alien[n].calcAngleMoveY(ang));
			alien[n].setLives(0);
			alien[n].setBounds(updateBounds(alien[n].getPolygon(), alien[n].getMoveAngle(), new Point.Double(alien[n].getX(),alien[n].getY())));
		}
		addKeyListener(this);
	}
	
	public void keyTyped(KeyEvent e) { }
	
	public void keyPressed(KeyEvent e) 
	{
		int key = e.getKeyCode();
		
		switch(key) 
		{
			case KeyEvent.VK_LEFT:
			{
				leftKey = true;
				break;
			}
			case KeyEvent.VK_RIGHT:
			{
				rightKey = true;
				break;
			}
			case KeyEvent.VK_UP:
			{
				upKey = true;
				player.moveShip(-90);
				break;
			}
			case KeyEvent.VK_DOWN:
			{
				downKey = true;
				player.moveShip(+90);
				break;
			}
			case KeyEvent.VK_CONTROL:
			case KeyEvent.VK_ENTER:
			case KeyEvent.VK_SPACE:
			{
				if (!player.isAlive())
					break;
				spaceKey = true;
				player.fireBullet();
				break;
			}
				
		}

	}

	public void keyReleased(KeyEvent e) 
	{
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
		{
			leftKey = false;
		}
		else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
		{
			rightKey= false;
		}
		else if (e.getKeyCode() == KeyEvent.VK_UP)
		{
			upKey = false;
		}
		else if (e.getKeyCode() == KeyEvent.VK_DOWN)
		{
			downKey = false;
		}
		else if (e.getKeyCode() == KeyEvent.VK_SPACE)
		{
			spaceKey = false;
		}

	}

	public void run() 
	{
		Thread t = Thread.currentThread();
		while (t == gameloop) {
			try {
				if (Math.abs(tickTimer - System.currentTimeMillis()) >= 1000)
				{
					TPS = ticks;
					ticks = 0;
					tickTimer = System.currentTimeMillis();
				}
				gameUpdate();
				ticks++;
				Thread.sleep(1000 / targetTPS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			repaint();
		}
	}

	public void update(Graphics g) 
	{
		g2d.setTransform(identity);
		g2d.setPaint(Color.BLACK);
		g2d.fillRect(0, 0, getSize().width, getSize().height);
			drawStars();
			drawAliens();
			drawShip();
			drawBullets();
			drawAsteroids();
			drawBulletCharge();
			drawInformationPanel();
		paint(g);	
	}
	
	private void drawBulletCharge()
	{
		g2d.setTransform(identity);
		g2d.setColor(new Color(82,255,255,255));	
        for(int i = 2; i < 7; i++)
        {
        	g2d.drawLine(screenWidth - 5 * i, screenHeight - 10, screenWidth - 5 * i, screenHeight - 30);
        }	
        g2d.setColor(new Color(30,89,82,255));
        for(int i = 6; i > (player.getBulletCharge() / 2) + 1; i--)
        {
        	g2d.drawLine(screenWidth - 5 * i, screenHeight - 10, screenWidth - 5 * i, screenHeight - 30);
        }
	}

	private void drawInformationPanel() 
	{
		
		g2d.setTransform(identity);
		g2d.setColor(Color.CYAN);
		informationPanelTopTwo.setY(screenHeight);
		g2d.draw(informationPanelTopTwo.getShape());
		g2d.setColor(informationPanelTopTwo.getColor());
		g2d.fill(informationPanelTopTwo.getShape());
		
		
		g2d.setColor(Color.CYAN);
		g2d.draw(informationPanelTop.getShape());
		g2d.setColor(informationPanelTop.getColor());
		g2d.fill(informationPanelTop.getShape());
		g2d.setColor(Color.WHITE);
		g2d.drawString(
				"Ship: " + Math.round(player.getX()) + ","
						+ Math.round(player.getY()), 520, informationPanelTop.getHeight() - 9);
		g2d.drawString("Face angle: " + Math.round(player.getFaceAngle()), 400, informationPanelTop.getHeight() - 9);
		g2d.drawString("Horizontal Velocity: " + Math.abs((int) (player.getVelX() * 100)), 200, informationPanelTop.getHeight() - 9);
		g2d.drawString("Vertical Velocity: " + Math.abs((int) (player.getVelY() * 100)), 40, informationPanelTop.getHeight() - 9);
		g2d.drawString("Ticks Per Second: " + TPS, 0, screenHeight);
	}
		
	private void drawStars() 
	{
			for (int n = 0; n < STARS; n++) {
						g2d.setTransform(identity);
						g2d.setColor(star[n].getColor());
						g2d.drawOval((int) star[n].getX(), (int) star[n].getY(), star[n].getRadius(), star[n].getRadius());
						g2d.setColor(Color.WHITE);
						g2d.fillOval((int) star[n].getX(), (int) star[n].getY(), star[n].getRadius(), star[n].getRadius());
				}
		
	}

	private void drawAsteroids() 
	{
		for (int n = 0; n < ASTEROIDS; n++) {
			if (ast[n].isAlive()) 
			{
				g2d.setTransform(identity);
				g2d.translate(ast[n].getX(), ast[n].getY());
				g2d.rotate(Math.toRadians(ast[n].getMoveAngle()));
				g2d.setColor(ast[n].getColor());
				g2d.fill(ast[n].getShape());
				if (showBounds)
					drawBounds(ast[n].getBounds());
			}
		}
	}

	private void drawBullets() {
		for (int m = 0; m < ALIENS; m++)
		{
			for (int n = 0; n < alien[m].getBulletCap(); n++) {
				if (alien[m].getBullet(n).isAlive()) 
				{
					g2d.setTransform(identity);
					g2d.translate(alien[m].getBullet(n).getX(), alien[m].getBullet(n).getY());
					g2d.setColor(alien[m].getBulletColor());
					g2d.draw(alien[m].getBullet(n).getShape());
				}
			}
		}
		for (int n = 0; n < player.getBulletCap(); n++) 
		{
			if (player.getBullet(n).isAlive()) 
			{
				g2d.setTransform(identity);
				g2d.translate(player.getBullet(n).getX(), player.getBullet(n).getY());
				g2d.setColor(player.getBulletColor());
				g2d.draw(player.getBullet(n).getShape());
			}
		}
	}

	private void drawShip() 
	{
		if(!player.isAlive())
			return;
		if (leftKey)
			player.incFaceAngle(-5);
		else if (rightKey)
			player.incFaceAngle(5);
		if (upKey) 
			player.moveShip(-90);
		else if (downKey)
			player.moveShip(90);
		if (spaceKey)
			player.fireBullet();
		g2d.setTransform(identity);
		g2d.translate(player.getX(), player.getY());
		g2d.rotate(Math.toRadians(player.getFaceAngle()));
		g2d.setColor(Color.LIGHT_GRAY);
		g2d.fill(player.getShape());
		g2d.setColor(Color.CYAN);
		g2d.draw(player.getShape());
		if (showBounds)
			drawBounds(player.getBounds());
	}
	
	private void drawAliens() 
	{
		for (int n = 0; n < ALIENS; n++)
		{
			if(alien[n].isAlive())
			{
				g2d.setTransform(identity);
				g2d.translate(alien[n].getX(), alien[n].getY());
				g2d.rotate(Math.toRadians(alien[n].getFaceAngle()));
				g2d.setColor(Color.LIGHT_GRAY);
				g2d.fill(alien[n].getShape());
				g2d.setColor(Color.RED);
				g2d.draw(alien[n].getShape());
				if (showBounds)
					drawBounds(alien[n].getBounds());
			}
		}
	}
	
	private void drawBounds(Shape bounds)
	{
		g2d.setTransform(identity);
		g2d.setColor(Color.GREEN);
		g2d.draw(bounds);
	}

	public void paint(Graphics g) 
	{
		g.drawImage(backbuffer, 0, 0, this);
	}

	public void start() 
	{
		gameloop = new Thread(this);
		gameloop.start();
	}
	
	public static void playSound(String name)
	{
		String path = "/Users/robinsonpa/Desktop/ComputerSci/AsteroidHunter/";
		try {
			 sound = AudioSystem.getAudioInputStream(new File(path + name));
			 audioPlayer = AudioSystem.getClip();
			 audioPlayer.open(sound);
			 audioPlayer.loop(0);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	private void gameUpdate() {
			updateBulletCharge();
			updatePlayer();
			updateAliens();
			updateStars();
			updateBullets();
			updateAsteroids();
			checkCollisions();
	}

	private void checkCollisions() 
	{
		for (int m = 0; m < ALIENS; m++)
		{
			if (alien[m].isAlive())
			{
				for (int n = 0; n < ALIENS; n++)
				{
					if(alien[n].isAlive())
					{
						if(n != m)
						{
							if (BaseVectorShape.intersects(alien[m].getBounds(),alien[n].getBounds())) 
							{
								playSound("Explosion.wav");
								alien[n].setAlive(false);
								alien[m].setAlive(false);
								alien[m].incLives(-1);
								alien[n].incLives(-1);
								continue;
							}
							for (int b = 0; b < alien[n].getBulletCap(); b++)
							{
								if (alien[n].getBullet(b).isAlive())
								{
									if (BaseVectorShape.intersects(alien[m].getBounds(),(alien[n].getBullet(b).getBounds())))
									{
										playSound("Explosion.wav");
										alien[m].setAlive(false);
										alien[n].getBullet(b).setAlive(false);
										alien[m].incLives(-1);
										continue;
									}
								}
							}
						}
					}
				}
				for (int n = 0; n < ASTEROIDS; n++)
				{
					if(ast[n].isAlive())
					{
						if(BaseVectorShape.intersects(alien[m].getBounds(),ast[n].getBounds()))
						{
							playSound("Explosion.wav");
							alien[m].setAlive(false);
							alien[m].incLives(-1);
							ast[n].incHealth(-1);
							if(ast[n].getHealth() <= 0)
								ast[n].setAlive(false);			
						}
					}
				}
				for (int n = 0; n < player.getBulletCap(); n++)
				{
					if(player.getBullet(n).isAlive())
					{
						if(BaseVectorShape.intersects(player.getBullet(n).getBounds(),(alien[m].getBounds())))
						{
							playSound("Explosion.wav");
							alien[m].setAlive(false);
							alien[m].incLives(-1);
							player.getBullet(n).setAlive(false);
							score+= 10;
							continue;
						}
					}
				}
				if (player.isAlive())
				{
					if(BaseVectorShape.intersects(player.getBounds(),(alien[m].getBounds())))
					{
						playSound("Explosion.wav");
						alien[m].setAlive(false);
						alien[m].incLives(-1);
						player.setAlive(false);
						player.incLives(-1);
						if (player.getLives() >= 1)
						{
							player.setX(320);
							player.setY(240);
							player.setFaceAngle(0);
							player.setVelX(0);
							player.setVelY(0);
							player.setAlive(true);
						}
					}
				}
			}
			for (int n = 0; n < alien[m].getBulletCap(); n++)
			{
				if(alien[m].getBullet(n).isAlive())
				{
					if(BaseVectorShape.intersects(player.getBounds(),(alien[m].getBullet(n).getBounds())))
					{
						playSound("Explosion.wav");
						alien[m].getBullet(n).setAlive(false);
						player.setAlive(false);
						player.incLives(-1);
						if (player.getLives() >= 1)
						{
							player.setX(320);
							player.setY(240);
							player.setFaceAngle(0);
							player.setVelX(0);
							player.setVelY(0);
							player.setAlive(true);
						}
					}
					for (int a = 0; a < ASTEROIDS; a++)
					{
						if(ast[a].isAlive())
						{
							if(BaseVectorShape.intersects(ast[a].getBounds(),(alien[m].getBullet(n).getBounds())))
							{
								playSound("Explosion.wav");
								alien[m].getBullet(n).setAlive(false);
								ast[a].incHealth(-1);
								if(ast[a].getHealth() <= 0)
									ast[a].setAlive(false);
							}
						}
					}
				}
			}
		}
		for (int a = 0; a < ASTEROIDS; a++)
		{
			if(ast[a].isAlive())
			{
				if(player.isAlive())
				{
					if(BaseVectorShape.intersects(player.getBounds(),ast[a].getBounds()))
					{
						playSound("Explosion.wav");
						ast[a].incHealth(-1);
						if(ast[a].getHealth() <= 0)
						{
							ast[a].setAlive(false);
						}
						player.setAlive(false);
						player.incLives(-1);
						if (player.getLives() >= 1)
						{
							player.setX(320);
							player.setY(240);
							player.setFaceAngle(0);
							player.setVelX(0);
							player.setVelY(0);
							player.setAlive(true);
						}
					}
				}
				for (int m = 0; m < player.getBulletCap(); m++)
				{
					if (player.getBullet(m).isAlive())
					{
						if (BaseVectorShape.intersects(player.getBullet(m).getBounds(),(ast[a].getBounds())))
						{
							playSound("Explosion.wav");
							player.getBullet(m).setAlive(false);
							ast[a].incHealth(-1);
							if(ast[a].getHealth() <= 0)
							{
								ast[a].setAlive(false);
							}
						}
					}
				}
					
			}
		}
		
	}
	
	private void updateAliens()
	{
		for (int n = 0; n < ALIENS; n++)
		{
			if (alien[n].isAlive())
			{
				alien[n].incX(alien[n].getVelX());
				if (alien[n].getX() < -10)
					alien[n].setX(getSize().width + 10);
				else if (alien[n].getX() > getSize().width + 10)
					alien[n].setX(-10);

				alien[n].incY(alien[n].getVelY());
				if (alien[n].getY() < -10)
					alien[n].setY(getSize().height + 10);
				else if (alien[n].getY() > getSize().height + 10)
					alien[n].setY(-10);
		
				int y = (int) (player.getY() - alien[n].getY());
				int x = (int) (player.getX() - alien[n].getX());
				double angleInDegrees = 90 + (Math.atan2(y, x) * 180 / Math.PI);
				if (angleInDegrees < 0)
					angleInDegrees = angleInDegrees + 360;
				double anglediff = (angleInDegrees - alien[n].getFaceAngle() + 180) % 360 - 180;
				if (anglediff >= 0)
					alien[n].incFaceAngle(3);
				else
					alien[n].incFaceAngle(-3);
				if (anglediff >= -4 && anglediff <= 4)
					alien[n].fireBullet();
				alien[n].moveShip(-90);
					
				alien[n].setVelX(alien[n].getVelX() * 0.99);
				alien[n].setVelY(alien[n].getVelY() * 0.99);
				
				alien[n].setBounds(updateBounds(alien[n].getPolygon(), alien[n].getFaceAngle(), new Point.Double(alien[n].getX(),alien[n].getY())));
			}
		}
	}
	
	private void updateBulletCharge()
	{
		for (int m = 0; m < ALIENS; m++)
		{
			if (Math.abs(alien[m].getBulletTimer() - System.currentTimeMillis()) >= 500)
			{
				alien[m].setBulletTimer(System.currentTimeMillis());
				if (alien[m].getBulletCharge() < 10)
						alien[m].incBulletCharge(0.5);
			}
		}
		if (Math.abs(player.getBulletTimer() - System.currentTimeMillis()) >= 500)
		{
			player.setBulletTimer(System.currentTimeMillis());
			if (player.getBulletCharge() < 10)
				if (!spaceKey)
					player.incBulletCharge(1);
		}	
	}
	
	private void updateStars() 
	{
		for (int n = 0; n < STARS; n++) {
				star[n].incX(Star.svx);

				if (star[n].getX() < -20)
					star[n].setX(getSize().width + 20);
				else if (star[n].getX() > getSize().width + 20)
					star[n].setX(-20);

				star[n].incY(Star.svy);
				
				if (star[n].getY() < -20)
					star[n].setY(getSize().height + 20);
				else if (star[n].getY() > getSize().height + 20)
					star[n].setY(-20);
				
				if (ticks % 2 == 0)
				{
					star[n].setSVY(-player.getVelY() * 0.9);
					star[n].setSVX(-player.getVelX() * 0.9);
				}
		}
		
	}
	
	private void updateAsteroids() {
		for (int n = 0; n < ASTEROIDS; n++) {

			if (ast[n].isAlive()) {
				ast[n].incX(ast[n].getVelX());

				if (ast[n].getX() < -20)
					ast[n].setX(getSize().width + 20);
				else if (ast[n].getX() > getSize().width + 20)
					ast[n].setX(-20);

				ast[n].incY(ast[n].getVelY());

				if (ast[n].getY() < -20)
					ast[n].setY(getSize().height + 20);
				else if (ast[n].getY() > getSize().height + 20)
					ast[n].setY(-20);

				ast[n].incMoveAngle(ast[n].getRotVel());

				if (ast[n].getMoveAngle() < 0)
					ast[n].setMoveAngle(360 - ast[n].getRotVel());
				else if (ast[n].getMoveAngle() > 360)
					ast[n].setMoveAngle(ast[n].getRotVel());
				
				ast[n].setBounds(updateBounds(ast[n].getPolygon(), ast[n].getMoveAngle(), new Point.Double(ast[n].getX(),ast[n].getY())));
			}
		}

	}

	private void updateBullets() {
		for (int m = 0; m < ALIENS; m++)
		{
			for (int n = 0; n < alien[m].getBulletCap(); n++) {
				
				if (alien[m].getBullet(n).isAlive()) 
				{

					alien[m].getBullet(n).incX(alien[m].getBullet(n).getVelX());

					if (alien[m].getBullet(n).getX() < 0 || alien[m].getBullet(n).getX() > getSize().width) 
					{
						alien[m].getBullet(n).setAlive(false);
					}

					alien[m].getBullet(n).incY(alien[m].getBullet(n).getVelY());

					if (alien[m].getBullet(n).getY() < 0 || alien[m].getBullet(n).getY() > getSize().height) 
					{
						alien[m].getBullet(n).setAlive(false);
					}
					alien[m].getBullet(n).setBounds(updateBounds(alien[m].getBullet(n).getPolygon(), alien[m].getBullet(n).getMoveAngle(), new Point.Double(alien[m].getBullet(n).getX(),alien[m].getBullet(n).getY())));
				}
			}
			
		}
		for (int n = 0; n < player.getBulletCap(); n++) {
			
			if (player.getBullet(n).isAlive()) 
			{

				player.getBullet(n).incX(player.getBullet(n).getVelX());

				if (player.getBullet(n).getX() < 0 || player.getBullet(n).getX() > getSize().width) 
				{
					player.getBullet(n).setAlive(false);
				}

				player.getBullet(n).incY(player.getBullet(n).getVelY());

				if (player.getBullet(n).getY() < 0 || player.getBullet(n).getY() > getSize().height) 
				{
					player.getBullet(n).setAlive(false);
				}		
				player.getBullet(n).setBounds(updateBounds(player.getBullet(n).getPolygon(), player.getBullet(n).getMoveAngle(), new Point.Double(player.getBullet(n).getX(),player.getBullet(n).getY())));	
			}
		}
	}

	private void updatePlayer() {
		if(!player.isAlive())
			return;
		player.incX(player.getVelX());

		if (player.getX() < -10)
			player.setX(getSize().width + 10);
		else if (player.getX() > getSize().width + 10)
			player.setX(-10);

		player.incY(player.getVelY());

		if (player.getY() < -10)
			player.setY(getSize().height + 10);
		else if (player.getY() > getSize().height + 10)
			player.setY(-10);
		
		player.setVelX(player.getVelX() * 0.99);
		player.setVelY(player.getVelY() * 0.99);
		
		player.setBounds(updateBounds(player.getPolygon(), player.getFaceAngle(), new Point.Double(player.getX(),player.getY())));
		
	}
	
	private Polygon updateBounds(Polygon polygon, double theta, Point.Double location)
	{
		if (polygon == null)
			return null;
		int[] x = polygon.xpoints;
		int[] y = polygon.ypoints;
		int[] rotatedX = new int[x.length];
		int[] rotatedY = new int[y.length];
		for(int i = 0 ; i < polygon.npoints; i++)
		{
		  double newx = x[i] * Math.cos(Math.toRadians(theta)) - y[i] * Math.sin(Math.toRadians(theta));
		  double newy = x[i] * Math.sin(Math.toRadians(theta)) + y[i]* Math.cos(Math.toRadians(theta));
		  rotatedX[i]=(int) (newx + location.getX());
		  rotatedY[i]=(int) (newy + location.getY());
		}
		return new Polygon(rotatedX, rotatedY, rotatedX.length);
		
	}
}
import java.awt.Color;
import java.awt.Polygon;

public class Star extends BaseVectorShape {

	//define the ship polygon
	int radius;
    private int[] starx = new int[4];
    private int[] stary = new int[4];
    Color color;
    public static double svx = 1;
	public static double svy = 1;

	public Star() {
        if ((int) (Math.random() * 11) == 1)
        	radius = 3;
        else if ((int) (Math.random() * 11) <= 4)
        	radius = 2;
        else
        	radius = 1;
        //sets the correct points for the dimensions depending on the radius
        for (int i = 0; i < starx.length; i++)
        {
        	starx[i] = radius;
        	stary[i] = radius;
        	
        	if (i == 1)
        		stary[i] = -radius;
        	if (i == 2)
        		starx[i] = -radius;
        	if (i == 3)
        	{
        		starx[i] = -radius;
        		stary[i] = -radius;
        	}
        } 
        setColor();
        setShape(new Polygon(starx, stary, starx.length));
        setAlive(false);
	}
	
	public int getRadius()
	{
		return radius;
	}

	public Color getColor() {
		return color;
	}
	public void setColor()
	{
		int a = (int)(Math.random() * 100) + 1;
		if (radius ==1 || a > 30)
			color = Color.WHITE;
		else if (a > 20)
			color = Color.CYAN;
		else if (a > 4)
			color = Color.BLUE;
		else if (a > 3)
			color = Color.ORANGE;
		else if (a > 2)
			color = Color.BLUE;
		else if (a > 1)
			color = Color.GREEN;
		else
			color = Color.MAGENTA;
	}
	public void setSVX(double a)
	{
		svx = a;
	}
	public void setSVY(double a)
	{
		svy = a;
	}
	
	

}

import java.awt.Color;
import java.awt.Polygon;

public class Ship extends BaseVectorShape 
{

    private int[] shipx = { -6, -3, 0, 3, 6, 0 };
    private int[] shipy = { 6, 8, 7, 8,6, -7 };
    public int lives;
    public double maxVelocity;
    public int currentBullet;
    public double bulletCharge;
    public int bulletCap;
    private Bullet[] bullet;
	long bulletTimer;
	long bulletSpacer;
	public Color bulletColor;
	public Polygon bounds;

	public Ship(int lives, int bulletCap, double maxVelocity, Color bulletColor) 
	{
		setShape(new Polygon(shipx, shipy, shipx.length));
		setPolygon(new Polygon(shipx, shipy, shipx.length));
        setAlive(true);
        bullet = new Bullet[bulletCap];
        for (int n = 0; n < bulletCap; n++) 
		{
			bullet[n] = new Bullet();
		}
        this.lives = lives;
        this.bulletCap = bulletCap;
        this.bulletColor = bulletColor;
        this.maxVelocity = maxVelocity;
	}
	
    public Polygon getBounds() 
    {
    	return bounds;
    }
	
	public int getLives()
	{
		return lives;
	}
	public void setLives(int a)
	{
		lives = a;
	}
	
	public void incLives(int a)
	{
		lives += a;
	}
	public void incFaceAngle(double i) 
	 { 
	    	this.setFaceAngle(this.getFaceAngle() + i);
	    	if (this.getFaceAngle() < 0) 
				this.setFaceAngle(360-5);
			else if (this.getFaceAngle() > 360) 
				this.setFaceAngle(5);
	}
	public void incVelY(double i) 
    { 
    		if (this.getVelY() >= -maxVelocity && this.getVelY() <= maxVelocity)	
    			this.setVelY(this.getVelY() + i);  
    }
	public void incVelX(double i) 
    { 
    		if (this.getVelX() >= -maxVelocity && this.getVelX() <= maxVelocity)	
    			this.setVelX(this.getVelX() + i);  
    }
	
	public void moveShip(int degree)
	{
		this.setMoveAngle(this.getFaceAngle() + degree);
		this.incVelX(this.calcAngleMoveX(this.getMoveAngle()));
		this.incVelY(this.calcAngleMoveY(this.getMoveAngle()));

	}
	
	public void fireBullet()
	{
		if (Math.abs(bulletSpacer - System.currentTimeMillis()) < 150)
			return;
		if (bulletCharge <= 0)
			return;
		bulletSpacer = System.currentTimeMillis();
		bulletCharge -= 2;
		currentBullet++;
		Asteroids.playSound("Laser.wav");
		if (currentBullet > bulletCap - 1)
				currentBullet = 0;
		bullet[currentBullet].setAlive(true);
		bullet[currentBullet].setX(this.getX());
		bullet[currentBullet].setY(this.getY());
		bullet[currentBullet].setMoveAngle(this.getFaceAngle() - 90);
		
		double angle = bullet[currentBullet].getMoveAngle();
		bullet[currentBullet].setVelX(Bullet.svx * calcAngleMoveX(angle) * 2);
		bullet[currentBullet].setVelY(Bullet.svy * calcAngleMoveY(angle) * 2);
	}
	public double getBulletCharge()
	{
		return bulletCharge;
	}
	public int getBulletCap()
	{
		return bulletCap;
	}
	public Bullet getBullet(int n)
	{
		return bullet[n];
	}
	public long getBulletTimer()
	{
		return bulletTimer;
	}
	public long getBulletSpacer()
	{
		return bulletSpacer;
	}
	public void incBulletCharge(double d) {
		bulletCharge += d;
		
	}
	public void setBulletTimer(long i) {
		bulletTimer = i;
		
	}
	public Color getBulletColor()
	{
		return bulletColor;
	}
	public void setBounds(Polygon polygon)
	{
		this.bounds = polygon;
	}
}

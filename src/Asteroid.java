import java.awt.Color;
import java.awt.Polygon;
import java.awt.Shape;

public class Asteroid extends BaseVectorShape {

	/**define the asteroid polygon shape*/
	private int[] astx = {-20,-17,-15,-13,-10,-7,-3, 0, 5, 10 ,14, 20,21, 22,23, 25,27,22, 20, 12,8, 2,-10,-18,-17,-16,-17,-18,-19};
    private int[] asty = { 20, 21, 22, 23, 22, 20,18,17,19,20, 21, 20,18, 16,13, 10,0,-8, -20,-22,-18, -14,-17,-13,-9, -5,-2,0,10};
    public Polygon bounds;
    
    public Color color;
 
    private double rotVel;
    
    public int health;

    public Asteroid() { 
    	setColor();
    	randomizePonts();
    	setShape(new Polygon(astx, asty, astx.length));
    	setPolygon(new Polygon(astx, asty, astx.length));
        setAlive(true);
        setRotVel(0.0);
        health = 1;
    }
    
    public void randomizePonts()
    {
    	for (int i = 0; i < astx.length; i++)
    	{
    		if (astx[i] >= 1)
    		{
    			if (Math.random() * 10 > 5)
    				astx[i] += 1;
    			else
    				astx[i] -= 1;
    		}
    		else
    		{
    			if (Math.random() * 10 > 5)
    				astx[i] += 1;
    			else
    				astx[i] -= 1;
    		}
    		if (asty[i] >= 1)
    		{
    			if (Math.random() * 10 > 5)
    				asty[i] += 1;
    			else
    				asty[i] -= 1;
    		}
    		else
    		{
    			if (Math.random() * 10 > 5)
    				asty[i] += 1;
    			else
    				asty[i] -= 1;
    		}
    		
    			
    	}
    }
   
    
    public double getRotVel() {
		return rotVel;
	}

	public void setRotVel(double rotVel) {
		this.rotVel = rotVel;
	}
	
    public Polygon getBounds() 
    {
    	return bounds;
    }

	public void setColor()
	{
		int a = (int) (Math.random() * 100) + 1;
		int blue = (int) (Math.random() * 11);
		if (a > 80)
			color = new Color(109,107,112 + blue,255); //Light Grey
		else if (a > 60)
			color = new Color(75,75,80 + blue,255); //Medium Grey
		else if (a > 40)
			color = new Color(60,60,65 + blue,255); //Light brown
		else if (a > 20)
			color = new Color(89,89,94 + blue,255);
		else
			color = new Color (45,45,50 + blue, 255);;	
	}
	
	public Color getColor() 
	{
		return color;
	}
	
	public int getHealth()
	{
		return health;
	}
	
	public void incHealth(int i) 
	{
		health = health + i;
		
	}
	public void setBounds(Polygon polygon) 
	{
		this.bounds = polygon;		
	}
    
}

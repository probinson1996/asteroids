import java.awt.Color;
import java.awt.Polygon;


public class InformationPanel extends BaseVectorShape {

	/**define the asteroid polygon shape*/
	private int[] astxTop = {40,0,640,600};
    private int[] astyTop = {25,0,0,25};
    private int[] astxTopTwo = {160,120,520,480};
    private int[] astyTopTwo = {50,25,25,50};
    int width;
    int height;
    public Color color;

    //default constructor
    public InformationPanel(String name) { 
    	color = new Color(32,91,133,100);
    	width = 640;
        height = 25;
        setAlive(true);
    	if (name.equals("Top"))
    	{
    		setShape(new Polygon(astxTop, astyTop, astxTop.length));
    	}
    	else
    	{
    		setShape(new Polygon(astxTopTwo, astyTopTwo, astxTopTwo.length));
    	}
    	
    }
   
    public Color getColor()
    {
    	return color;
    }
    public int getWidth()
    {
    	return width;
    }
    public int getHeight()
    {
    	return height;
    }


    
}

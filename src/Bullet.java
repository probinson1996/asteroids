import java.awt.Polygon;
import java.awt.Shape;


public class Bullet extends BaseVectorShape {
	public static double svx;
	public static double svy;
	private int[] bulletx = {0,1, 1, 0};
    private int[] bullety = {0,0, -1, -1};
    private Polygon bounds;
    public Bullet() {
        //create the bullet shape
    	setShape(new Polygon(bulletx, bullety, bulletx.length));
    	setPolygon(new Polygon(bulletx, bullety, bulletx.length));
        setAlive(false);
        svx = 1.5;
        svy = 1.5;
    }
    public Polygon getBounds() {
        return bounds;
    }
    public void setBounds(Polygon polygon)
    {
    	this.bounds = polygon;
    }


}
